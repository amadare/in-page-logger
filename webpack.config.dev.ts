import * as webpack from "webpack"
import * as HtmlWebpackPlugin from "html-webpack-plugin";

export = {
    entry: {
        "index" : "./dev.index.js"
    },
    mode: 'development',
    output: {
        path: __dirname + '/dist',
        filename: 'html-logging.js'
    },
    module: {
        rules: [
            { test: /.*\.css$/, loader: "raw-loader" }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./index.html",
            chunks: ["index"]
        })
    ]
} as webpack.Configuration;