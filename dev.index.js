import { Logger } from "./index";

var logger = new Logger({
    container: document.getElementById("logsContainer"),
    clearButton: document.getElementById("cleanLogs")
});
logger.shim();
document.querySelector("#console").addEventListener("click", (e) => {
    console.log("from console", 42, 14, e);
});
document.querySelector("#logger").addEventListener("click", (e) => {
    logger.log("test", ["from logger", 42, 14, e]);
});
document.querySelector("#orientation").addEventListener("click", (e) => {
    var orientation = logger.appendToTop = !logger.appendToTop;
    console.log("appendToTop:", orientation);
});
document.querySelector("#long").addEventListener("click", (e) => {
    console.log("aslkdf jaslkdjfh jkahsdfkj hakjsdhf kjhaskjdhf jkahsdkljf hakjsdhfk ahksdfh kajshdf kjahsdfkl jhasdklf jhasdklf jhaskdjfh iawehf oasjdhf kljqwheoifuh askdlfj hqioweuhf kajsdhfo iqwuhefk ljahsd");
});