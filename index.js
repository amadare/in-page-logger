import JSONView from "json-view";
import loggingStyleText from "./style.css";
import viewStyleText from "json-view/devtools.css";

var styleText = loggingStyleText + viewStyleText;

export class Logger {

    constructor(options) {
        var ul = document.createElement("ul");
        ul.classList.add("logger-inner-container");
        options.container.appendChild(ul);
        this.container = ul;
        this.ensureStyle();
        this.appendToTop = options.appendToTop === undefined ? true : options.appendToTop;
        if (options.clearButton) options.clearButton.addEventListener("click", () => this.clear());
    }

    ensureStyle() {
        var styles = document.querySelector("style.logger-inner-container-styles");
        if (styles) return;
        var element = document.createElement("style");
        element.classList.add("logger-inner-container-styles");
        element.innerHTML = styleText;
        document.head.appendChild(element);
    }

    shim() {
        var members = ["error", "log", "warn", "info", "dir", "groupStart", "groupCollapsed", "trace"];
        for (let member of members) {
            let _m = console[member];
            console[member] = (...args) => {
                _m(...args);
                this.log(member, args);
            };
        }
    }

    clear() {
        this.container.innerHTML = "";
    }

    log(level, args) {
        var node = this.getElement(level, args);
        var position = this.appendToTop ? "afterbegin" : "beforeend";
        this.container.insertAdjacentElement(position, node);
    }

    getTimeNode() {
        var node = document.createElement("span");
        node.classList.add("time");
        node.innerText = "[" + (new Date()).toLocaleTimeString() + "] ";
        return node;
    }

    getLevelNode(level) {
        var node = document.createElement("span");
        node.classList.add("level");
        node.innerText = `[${level.toUpperCase()}]`;
        return node;
    }

    getElement(level, args) {
        var node = document.createElement("li");
        node.appendChild(this.getTimeNode());
        node.appendChild(this.getLevelNode(level));
        for (var arg of args) {
            var view;
            if (arg instanceof Event) {
                view = new JSONView("Event", this.simpleKeys(arg));
                view.valueEditable = false;
                node.appendChild(view.dom);
            } else if (typeof arg === "object") {
                view = new JSONView("Object", this.simpleKeys(arg));
                view.valueEditable = false;
                node.appendChild(view.dom);
            } else {
                var p = document.createElement("span");
                p.classList.add("part");
                if (typeof arg === "number") {
                    p.classList.add("number");
                }
                p.innerText = arg;
                node.appendChild(p);
            }
        }
        if (level === "trace") {
            var stack = document.createElement("span");
            stack.innerText = this.getStack();
            node.appendChild(stack);
        }
        return node;
    }

    getStack() {
        var stack = new Error().stack;
        var lastLoggerIndex = stack.lastIndexOf(" at Logger.");
        var newLine = stack.substring(lastLoggerIndex).indexOf("\n") + lastLoggerIndex;
        return stack.substring(newLine);
    }

    simpleKeys (original) {
        var obj = {};
        for(var key in original) {
            obj[key] = original[key];
        }
        return obj;
    }
}
