import * as webpack from "webpack"

export = {
    entry: "./index.js",
    mode: 'production',
    output: {
        path: __dirname + '/dist',
        filename: 'html-logging.js',
        library: 'html-logging',
        libraryTarget: 'commonjs2'
    },
    module: {
        rules: [
            { test: /.*\.css$/, loader: "raw-loader" }
        ]
    }
} as webpack.Configuration;